# Proyecto del curso *Desarrollo del lado servidor: NodeJS, Express y MongoDB*

## App en Heroku
[red-bicicletas-py.herokuapp.com](https://red-bicicletas-py.herokuapp.com/)

**Usuario de Prueba**

_Usuario_: **prueba@prueba.com**

_Contraseña_: **123456**


## Recursos

**Templates**

[startbootstrap.com](https://startbootstrap.com/)

**Convertir html a pug**

[html-to-pug.com](https://html-to-pug.com/)

**Mapas**

[Leafletjs.com](https://leafletjs.com/)

**Servidor Produccion**

[Heroku.com](https://heroku.com/)

**DB Produccion**

[Mongodb.com](https://cloud.mongodb.com/)

**Email Produccion**

[Sendgrid.com](https://sendgrid.com/)

**APIS oAuth**

[Google Developers](https://developers.google.com/)

[Facebook Developers](https://developers.facebook.com/)